import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'weather',
        loadChildren: 'app/weather/weather.module#WeatherModule'
    },
    {
        path: 'climate-control',
        loadChildren: 'app/climate-control/climate-control.module#ClimateControlModule'
    },
    {
        path: 'monitoring',
        loadChildren: 'app/monitoring/monitoring.module#MonitoringModule'
    },
    {
        path: 'security',
        loadChildren: 'app/security/security.module#SecurityModule'
    },
    {
        path: 'entertainment',
        loadChildren: 'app/entertainment/entertainment.module#EntertainmentModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    declarations: [HomeComponent],
    exports: [RouterModule]
})
export class AppRoutingModule { }
