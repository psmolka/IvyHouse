import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntertainmentComponent } from './entertainment.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: EntertainmentComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EntertainmentComponent]
})
export class EntertainmentModule { }
