import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";

interface Unit {
  unit :string;
  name :string;
}

@Component({
  selector: 'weather-page',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  temp :number;
  humidity :number;
  pressure :number;
  updater :Subscription;
  location :string = 'Wrocław';
  tempUnit :string = 'C';
  units :Unit[] = [{
      unit: 'C',
      name: 'Celcius'
    }, {
      unit: 'F',
      name: 'Fahrenheit'
    }, {
      unit: 'K',
      name: 'Kelvin'
    }];

  constructor (private http: HttpClient) {
    this.updateWeatherData();
  }

  onKey(value :string) {
    this.location = value;
    this.updateWeatherData();
  }

  ngOnInit() {
    const dueTime = 0;
    const updatesInterval = 10000;
    let timer = TimerObservable.create(dueTime, updatesInterval);
    this.updater = timer.subscribe(t => {
      this.updateWeatherData();
    });
  }

  ngOnDestroy() {
    this.updater.unsubscribe();
  }

  updateWeatherData() {
    this.http.get('http://api.openweathermap.org/data/2.5/weather?id=524901&APPID=6ab0f0c3b70339bed4eecedcc3ec0f4d&q='+ this.location +',PL').subscribe((data :any) => {
      this.temp = data.main.temp;
      this.humidity = data.main.humidity;
      this.pressure = data.main.pressure;
    });
  }

  getTempInFahrenheit() {
    return Math.round(this.temp * (9/5) - 459.67);
  }
  getTempInKelvin() {
    return Math.round(this.temp);
  }

  getTempInCelcius() {
    return Math.round(this.temp - 273.15);
  }

  getTemp() {
    switch(this.tempUnit) {
      case 'C': return this.getTempInCelcius();
      case 'F': return this.getTempInFahrenheit();
      case 'K': return this.getTempInKelvin();
    default: return this.getTempInCelcius();
    }
  }
}
